import pickle
import pandas as pd
import torch
import numpy as np
from torch.utils.data import Dataset
from sklearn.preprocessing import KBinsDiscretizer, OneHotEncoder

class Anneal_dataset(Dataset):
    def __init__(self,file_name):

        n_feature = 38
        # file_name = 'hepatitis.data'
        headers = ['family','type','steel','carbon','hardness','temper','condition','formability',
                   'strength','non-ageing','surface-finish','surface-quality',
                   'enemelability','bc','bf','bt','bw','bl','m','chrom','phos','cbond','marvi','exptl','ferro',
                   'corr','blue','lustre','jurofm','s','p','shape','thick','width','len','oil','bore','packing'
                   ]
        data = pd.read_csv(file_name, header=None).to_numpy()
        class_position = 38
        Y = data[:, [class_position]]
        # Y = Y.astype(np.float32)
        string_pos = [0,1,2,5,6,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
                      31,35,36,37]
        for pos in string_pos:
            column = data[:,pos]
            s = set()
            for e in column:
                s.add(e)
            dict = {}
            count=0
            for e in s:
                dict[e] = count
                count +=1
            for i,row in enumerate(data):
                data[i][pos] = dict[data[i][pos]]
        non_binary_variable_pos = list(range(n_feature))  # after having removed the target
        dic_col_bin = {
            0: 2, 1:3, 2:9,3:8,4:8,5:2,6:4,7:6,8:8,9:2,10:3,11:5,12:6,13:2,14:2,15:2,16:3,17:2,18:2,19:2,20:2,21:2,
            22:2,23:2,24:2,25:2,26:5,27:2,28:2,29:2,30:2,31:2,32:8,33:8,34:8,35:3,36:4,37:4
        }
        data = np.delete(data, [class_position], 1)

        missing_values_locations = []
        # for i, row in enumerate(data):
        #     for j, v in enumerate(row):
        #         if v == '?':
        #             missing_values_locations.append((i, j))
        #             data[i, j] = np.NaN
        # data = data.astype(np.float32)
        # for i, row in enumerate(data):
        #     for j, v in enumerate(row):
        #         if (i, j) in missing_values_locations:
        #             data[i, j] = np.nanmin(data[:, j])


        dict_column_intervals = {}
        for c, column in enumerate(data.T):
            if c in non_binary_variable_pos:
                est = KBinsDiscretizer(n_bins=dic_col_bin[c], encode='ordinal', strategy='uniform')
                column = column.reshape(-1, 1)
                est.fit(column)
                # data[:,[c]] = est.transform(column)
                dict_column_intervals[c] = est.bin_edges_[0]

        total_length = sum([2 * dic_col_bin[v] for v in dic_col_bin.keys()])
                           # +
                           # [2 for v in range(n_feature) if v not in non_binary_variable_pos])
        final_dataset = np.ones((data.shape[0], total_length))
        ####
        for i, row in enumerate(data):
            pos = 0
            for j, v in enumerate(row):
                if j in non_binary_variable_pos:
                    length = 2 * dic_col_bin[j]
                    if (i, j) in missing_values_locations:
                        # final_dataset[i][pos:pos+length] = np.ones((length))
                        pos += length
                    else:
                        for k in range(dic_col_bin[j]):
                            interval_a, interval_b = dict_column_intervals[j][k:k + 2]
                            if interval_a <= v < interval_b:
                                final_dataset[i][pos + (2 * k) + 1] = 0
                            else:
                                final_dataset[i][pos + (2 * k)] = 0
                        pos += length
                else:
                    if (i, j) in missing_values_locations:
                        pos += 2
                    else:
                        if v == 1:
                            final_dataset[i][pos + 1] = 0
                        else:
                            final_dataset[i][pos] = 0
                        pos += 2

        self.predicates = []
        for i, _ in enumerate(data[0]):
            # pos = 0
            if i in non_binary_variable_pos:
                # length = 2 * dic_col_bin[i]
                for k in range(dic_col_bin[i]):
                    interval_a, interval_b = dict_column_intervals[i][k:k + 2]
                    self.predicates.append(headers[i] + f'_{interval_a}_{interval_b}')
                    self.predicates.append('not_' + headers[i] + f'_{interval_a}_{interval_b}')
                # pos+=length
            else:
                self.predicates.append(headers[i])
                self.predicates.append('not_' + headers[i])

        for i, row in enumerate(Y):
            if row[0]  == 'U' :
                Y[i][0] = 1
            else:
                Y[i][0] = 0
        Y = Y.astype(np.float32)
        self.rows = torch.tensor(final_dataset).float()
        self.labels = torch.tensor(Y).float()
        self.variables_number = final_dataset.shape[1]

    def __len__(self):
        return len(self.rows)

    def __getitem__(self, idx):
        example = self.rows[idx]
        label = self.labels[idx]
        return example, label

