import torch.optim
from torch.utils.data import DataLoader
from ray import tune
from possiblistic_nn.PossibilisticNN import *
from Hepatitis_dataset import *
import copy
import itertools
from termcolor import colored
import time

def printr(s):
    print(colored(s,'red'))


dataset = Hepatitis_dataset('hepatitis.data')
input_size = dataset.variables_number
print(input_size)
batch_size = 8

train_dataset_length = int(len(dataset) * 0.8)
train_set, val_set = torch.utils.data.random_split(dataset,
                                                   [train_dataset_length, len(dataset) - train_dataset_length])
validation_dataset_length = int(len(val_set)*0.5)
val_set, test_set = torch.utils.data.random_split(val_set,
                        [validation_dataset_length, len(val_set) - validation_dataset_length])
train_dataloader = DataLoader(train_set, batch_size=batch_size)
val_dataloader = DataLoader(val_set, batch_size=batch_size)
test_dataloader = DataLoader(test_set, batch_size=batch_size)
loss_fn =  nn.MSELoss(reduction='sum')


def objective(config):
    description = [input_size]
    for i in range(config["n_layers"]):
        description.append(config["nodes_layer"])
    description.append(config["nodes_output"])

    model = PossibilisticNN(description)
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr=config["learning_rate"], weight_decay=0.005)
    start = time.time()
    model, _, avg_valid_losses = train_model(
        config["epochs"],
        model,
        config["train_dataloader"],
        config["val_dataloader"],
        config["loss_fn"],
        optimizer, patience=10)
    end = (time.time() - start)
    score = sum(avg_valid_losses)/len(avg_valid_losses)
    return {"score": score, "model" : model, "time" : end}


# 2. Define a search space.
search_space = {
    "input_size": input_size,
    "epochs" : 100,
    "train_dataloader" : train_dataloader,
    "val_dataloader" : val_dataloader,
    "loss_fn" : loss_fn,
    "batch": tune.grid_search([8]),
    "learning_rate": tune.grid_search([0.01]),
    "n_layers": tune.grid_search([3]),
    "nodes_layer": tune.choice([30,]),
    "nodes_output": tune.grid_search([10]),

}


# 3. Start a Tune run and print the best result.
tuner = tune.Tuner(objective, param_space=search_space)
results = tuner.fit()
print(results.get_best_result(metric="score", mode="min").config)
model  = results.get_best_result(metric="score", mode="min").metrics["model"]

print()
test_loop(test_dataloader, model, loss_fn)
print()

predicates = dataset.predicates
for o in range(model.possibility_layers[-1].size_out):
    printr(f'rule number {o} :')
    for pos,conf in model.get_rule(rule_number=o):
        print(predicates[pos])
    print()

printr("++++++++++++++++++++++++++++++++++++++++++++")
print()
import wittgenstein as lw

x, y = train_set[:]
X_train = x.numpy().astype(int)
Y_train = y.numpy().astype(int)

tx = []
ty = []
for x in test_set:
    tx.append(x[0].numpy())
    ty.append(x[1].numpy())
test_X = np.array(tx)
test_y = np.array(ty)

ripper_clf = lw.RIPPER()

start = time.time()
ripper_clf.fit(X_train,Y_train)
print("Time: ", time.time()-start)
#
print("score: ", ripper_clf.score(test_X, test_y))

print()
ripper_clf.ruleset_.out_pretty()