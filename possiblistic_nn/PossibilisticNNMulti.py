import torch
from torch import nn

device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Possibilistic NN loaded.\nUsing {device} device")

def log_smooth_max(inp, alpha=30, dim=3):
    exp = torch.exp(alpha * inp)
    return (1 / alpha) * torch.log(torch.sum(exp, dim=dim))

def custom_max(input):
    while input.size()[2] != 1:
        n_examples = input.size()[0]
        size_in = input.size()[2]
        if size_in % 2 > 0:
            input = torch.cat((input, torch.zeros(n_examples, input.size()[1], 1)), 2)
            size_in += 1
        input = input.view(n_examples, input.size()[1], size_in // 2, 2)
        input = log_smooth_max(input, dim=3)
    return torch.squeeze(input,dim=2)

def custom_max_last_layer(input):
    while input.size()[1] != 1:
        n_examples = input.size()[0]
        size_in = input.size()[1]
        if size_in % 2 > 0:
            input = torch.cat((input, torch.zeros(n_examples,  1)), 1)
            size_in += 1
        input = input.view(n_examples,size_in // 2, 2)
        input = log_smooth_max(input, dim=2)
    return input


class PossibilityLayer(nn.Module):
    def __init__(self, size_in,size_out):
        super().__init__()

        self.size_in, self.size_out = size_in, size_out

        self.weight = nn.Parameter(torch.rand(size_out,size_in))

    def forward(self, x):
        x = torch.einsum('bi,ji->bji', x, self.weight)
        x = custom_max(x)
        # x = torch.clamp(x, min=0) # this is better
        return x

class PossibilisticNN(torch.nn.Module):
    """
    hidden_layers_nodes: [#v1, .... #vn] v_i and v_(i+1) is the input and the output of the ith layer, respectively.
    """

    def __init__(self, hidden_layers_nodes,  predicates=None, target_predicate=None, background_knowledge=None):
        super(PossibilisticNN, self).__init__()
        self.n_layers = len(hidden_layers_nodes) - 2
        self.n_output = hidden_layers_nodes[-1]

        self.possibility_layers = nn.ModuleList(
            [PossibilityLayer(hidden_layers_nodes[i], hidden_layers_nodes[i + 1])
             for i in range(self.n_layers)]
        )

        self.possibilities = nn.ParameterList(
            [
                nn.Parameter(torch.rand(hidden_layers_nodes[-2]))
                for i in range(self.n_output)
            ]
        )


    def forward(self, x):
        for i in range(len(self.possibility_layers)):
            x = self.possibility_layers[i](x)
        alpha = 50
        beta = -30

        out=torch.empty(x.shape[0],self.n_output)
        for i in range(self.n_output):
            a = (1/alpha)*torch.logaddexp(alpha*self.possibilities[i],alpha*x)
            # print(a)
            # print(a.shape)
            # print(((1 / beta) * torch.logsumexp(beta * a, dim=1, keepdim=True)).flatten())
            # print(((1/beta)*torch.logsumexp(beta*a, dim=1, keepdim=True)).shape)
            # print('out\n', out[:, i])
            out[:,i] = ((1/beta)*torch.logsumexp(beta*a, dim=1, keepdim=True)).flatten()
        # x= torch.amin(x,dim=1,keepdim=True)
        return out

    def get_rule(self,
                 rule_number = 0,
                 # input_predicates=None,
                 # target_name='target',
                 min_value_selection = 0.8):
        current_layer = len(self.possibility_layers) - 1
        current_layer_indexes = [(rule_number,1.)]
        next_layer_indexes = []
        while current_layer >= 0:
            for i,inclusion_level in current_layer_indexes:
                row = self.possibility_layers[current_layer].weight[i]
                for j, value in enumerate(row):
                    value = float(value.detach())
                    if value > min_value_selection:
                        next_layer_indexes.append([j,min(value,inclusion_level)])
            current_layer -= 1
            current_layer_indexes = next_layer_indexes
            next_layer_indexes = []

        # So far we have selected variables occuring
        # in the negated disjunction of the target conjunction,
        # therefore we specify the selected variables in the conjunction:
        included_variables = current_layer_indexes
        for i,(j,inclusion_level) in enumerate(current_layer_indexes):
            if j % 2 == 0:
                included_variables[i][0] = j+1
            if j % 2 > 0:
                included_variables[i][0] = j -1
        return included_variables


###########################
###########################
###########################
# TRAINING
###########################

import numpy as np
import torch

class EarlyStopping:
    def __init__(self, patience=4, delta=0.001, trace_func=print,verbose=True):

        self.patience = patience
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.val_loss_min = np.Inf
        self.delta = delta
        self.trace_func = trace_func
        self.verbose = verbose

    def __call__(self, val_loss, model):

        score = val_loss
        # print(f'MADONNA.\n score ; {score}, bestscore {self.best_score} , delta{self.delta}')
        if self.best_score is None:
            self.best_score = score
        elif score + self.delta > self.best_score:
            self.counter += 1
            if self.verbose:
                self.trace_func(f'EarlyStopping counter: {self.counter} out of {self.patience}')
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.counter = 0


def train_model(n_epochs, model, train_loader, valid_loader,   loss_fn, optimizer,patience=10,verbose=True):
    # to track the training loss as the model trains
    train_losses = []
    # to track the validation loss as the model trains
    valid_losses = []
    # to track the average training loss per epoch as the model trains
    avg_train_losses = []
    # to track the average validation loss per epoch as the model trains
    avg_valid_losses = []

    # initialize the early_stopping object
    early_stopping = EarlyStopping(patience=patience,verbose=verbose)

    for epoch in range(1, n_epochs + 1):
        ###################
        # train the model #
        ###################
        for batch, (data, target) in enumerate(train_loader):
            # clear the gradients of all optimized variables
            optimizer.zero_grad()
            # forward pass: compute predicted outputs by passing inputs to the model
            output = model(data)
            # calculate the loss
            loss = loss_fn(output, target)
            # backward pass: compute gradient of the loss with respect to model parameters
            loss.backward()
            # perform a single optimization step (parameter update)
            optimizer.step()
            # record training loss
            train_losses.append(loss.item())
            with torch.no_grad():
                for poss_values in model.possibilities:
                    torch.clamp(poss_values, min=0., max=1., out=poss_values)
                for layer in model.possibility_layers:
                    torch.clamp(layer.weight, min=.001, max=1., out=layer.weight)

        ######################
        # validate the model #
        ######################
        for data, target in valid_loader:
            # forward pass: compute predicted outputs by passing inputs to the model
            output = model(data)
            # calculate the loss
            loss = loss_fn(output, target)
            # record validation loss
            valid_losses.append(loss.item())


        # print training/validation statistics
        # calculate average loss over an epoch
        train_loss = np.average(train_losses)
        valid_loss = np.average(valid_losses)
        avg_train_losses.append(train_loss)
        avg_valid_losses.append(valid_loss)

        epoch_len = len(str(n_epochs))

        if verbose :
            print_msg = (f'[{epoch:>{epoch_len}}/{n_epochs:>{epoch_len}}] ' +
                         f'train_loss: {train_loss:.5f} ' +
                         f'valid_loss: {valid_loss:.5f}')

            print(print_msg)

        # clear lists to track next epoch
        train_losses = []
        valid_losses = []

        # early_stopping needs the validation loss to check if it has decresed,
        # and if it has, it will make a checkpoint of the current model
        early_stopping(valid_loss, model)


        if early_stopping.early_stop:
            if verbose:
                print("Early stopping")
            break


    return model, avg_train_losses, avg_valid_losses

def train(dataloader, model, loss_fn, optimizer):
    loss_h =  []
    size = len(dataloader.dataset)

    for batch, (X, y) in enumerate(dataloader):
        # Compute prediction and loss
        optimizer.zero_grad()
        pred = model(X)
        loss = loss_fn(pred, y)
        loss_h.append(loss.item())
        # Backpropagation
        loss.backward(retain_graph=False)
        optimizer.step()

        with torch.no_grad():
            torch.clamp(model.possibilities, min=0., max=1., out=model.possibilities)
            for layer in model.possibility_layers:
                torch.clamp(layer.weight, min=.001, max=1., out=layer.weight)

    print(sum(loss_h)/len(loss_h))
    return loss_h



def test_loop(dataloader, model, loss_fn):
    loss_h = []
    TP = 0
    TN = 0
    FP = 0
    FN = 0

    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            loss = loss_fn(pred, y)
            loss_h.append(loss.item())

            TP += ((pred >= 0.6) == (y >= 0.6)).float().sum()
            TN += ((pred < 0.6) == (y < 0.6)).float().sum()
            FP += ((pred >= 0.6) == (y < 0.6)).float().sum()
            FN += ((pred < 0.6) == (y >= 0.6)).float().sum()

    accuracy = (TP + TN)/(TP + TN + FP + FN)

    print(f'Accuracy: {accuracy}.\nTest loss: {sum(loss_h) / len(loss_h)}')
