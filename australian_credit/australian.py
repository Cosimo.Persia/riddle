import torch.optim
from torch.utils.data import DataLoader
from possiblistic_nn.PossibilisticNN import *
from australian_dataset import *
import copy
import itertools
from termcolor import colored
import time

def printr(s):
    print(colored(s,'red'))

h_dataset = Australian_dataset('australian.dat')
input_size = h_dataset.variables_number
print(input_size)
batch_size = 32

train_dataset_length = int(len(h_dataset)*0.8)
train_set, val_set = torch.utils.data.random_split(h_dataset,
                        [train_dataset_length, len(h_dataset) - train_dataset_length])
validation_dataset_length = int(len(val_set)*0.5)
val_set, test_set = torch.utils.data.random_split(val_set,
                        [validation_dataset_length, len(val_set) - validation_dataset_length])
train_dataloader = DataLoader(train_set, batch_size=batch_size)
val_dataloader = DataLoader(val_set, batch_size=batch_size)
test_dataloader = DataLoader(test_set, batch_size=batch_size)
loss_fn =  nn.MSELoss(reduction='sum')

epochs=100
description = [input_size,20,10,5]
model = PossibilisticNN(description)

optimizer = torch.optim.Adam(model.parameters(),
                             lr=.011, weight_decay=0.001)

start = time.time()
train_model(epochs, model, train_dataloader, val_dataloader, loss_fn, optimizer, patience=10)
print("TIME: ", time.time()-start)

print()
test_loop(test_dataloader, model, loss_fn)

print()
print()

predicates = h_dataset.predicates
for o in range(description[-1]):
    print(f'rule number {o}')
    for pos,conf in model.get_rule(rule_number=o):
        print(predicates[pos])
    print()

# printr("++++++++++++++++++++++++++++++++++++++++++++")
# print()
# import wittgenstein as lw
#
# x, y = train_set[:]
# X_train = x.numpy().astype(int)
# Y_train = y.numpy().astype(int)
#
# tx = []
# ty = []
# for x in test_set:
#     tx.append(x[0].numpy())
#     ty.append(x[1].numpy())
# test_X = np.array(tx)
# test_y = np.array(ty)
#
# ripper_clf = lw.RIPPER()
#
# start = time.time()
# ripper_clf.fit(X_train,Y_train)
# print("Time: ", time.time()-start)
# #
# print("score: ", ripper_clf.score(test_X, test_y))
#
# print()
# ripper_clf.ruleset_.out_pretty()