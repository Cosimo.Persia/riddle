import pickle
import pandas as pd
import torch
import numpy as np
from torch.utils.data import Dataset
from sklearn.preprocessing import KBinsDiscretizer, OneHotEncoder

class Auto_dataset(Dataset):
    def __init__(self,file_name):

        # file_name = 'hepatitis.data'
        headers = ['symboling','losses','make','fuel','aspiration','num-of-doors','body_style','drive_wheels',
                   'engine_location','wheel_base','length','width','height','curb_weight','engine_type',
                   'n_cylinders','engine_size','fuel_system','bore','stroke','compression_ratio',
                   'horsepower','peak_rpm','city_mpg','highway_mpg',
                   ]
        data = pd.read_csv(file_name, header=None).to_numpy()
        n_feature = 25
        class_position = 25
        Y = data[:, [class_position]]
        Y = Y.astype(np.float32)
        string_pos = [2, 3, 4, 5, 6, 7, 8, 14, 15, 17]
        for pos in string_pos:
            column = data[:,pos]
            s = set()
            for e in column:
                s.add(e)
            dict = {}
            count=0
            for e in s:
                dict[e] = count
                count +=1
            for i,row in enumerate(data):
                data[i][pos] = dict[data[i][pos]]

        non_binary_variable_pos = list(range(n_feature))  # after having removed the target
        dic_col_bin = {
            0: 6, 1:8, 2:21,3:2,4:2,5:2,6:5,7:3,8:2,9:8,10:8,11:8,12:8,13:8,14:7,15:7,16:8,17:8,18:8,19:8,20:8,21:8,
            22:8,23:8,24:8
        }
        data = np.delete(data, [class_position], 1)

        missing_values_locations = []
        for i, row in enumerate(data):
            for j, v in enumerate(row):
                if v == '?':
                    missing_values_locations.append((i, j))
                    data[i, j] = np.NaN
        data = data.astype(np.float32)
        for i, row in enumerate(data):
            for j, v in enumerate(row):
                if (i, j) in missing_values_locations:
                    data[i, j] = np.nanmin(data[:, j])


        dict_column_intervals = {}
        for c, column in enumerate(data.T):
            if c in non_binary_variable_pos:
                est = KBinsDiscretizer(n_bins=dic_col_bin[c], encode='ordinal', strategy='uniform')
                column = column.reshape(-1, 1)
                est.fit(column)
                # data[:,[c]] = est.transform(column)
                dict_column_intervals[c] = est.bin_edges_[0]

        total_length = sum([2 * dic_col_bin[v] for v in dic_col_bin.keys()])
                           # +
                           # [2 for v in range(n_feature) if v not in non_binary_variable_pos])
        final_dataset = np.ones((data.shape[0], total_length))
        ####
        for i, row in enumerate(data):
            pos = 0
            for j, v in enumerate(row):
                if j in non_binary_variable_pos:
                    length = 2 * dic_col_bin[j]
                    if (i, j) in missing_values_locations:
                        # final_dataset[i][pos:pos+length] = np.ones((length))
                        pos += length
                    else:
                        for k in range(dic_col_bin[j]):
                            interval_a, interval_b = dict_column_intervals[j][k:k + 2]
                            if interval_a <= v < interval_b:
                                final_dataset[i][pos + (2 * k) + 1] = 0
                            else:
                                final_dataset[i][pos + (2 * k)] = 0
                        pos += length
                else:
                    if (i, j) in missing_values_locations:
                        pos += 2
                    else:
                        if v == 1:
                            final_dataset[i][pos + 1] = 0
                        else:
                            final_dataset[i][pos] = 0
                        pos += 2

        self.predicates = []
        for i, _ in enumerate(data[0]):
            # pos = 0
            if i in non_binary_variable_pos:
                # length = 2 * dic_col_bin[i]
                for k in range(dic_col_bin[i]):
                    interval_a, interval_b = dict_column_intervals[i][k:k + 2]
                    self.predicates.append(headers[i] + f'_{interval_a}_{interval_b}')
                    self.predicates.append('not_' + headers[i] + f'_{interval_a}_{interval_b}')
                # pos+=length
            else:
                self.predicates.append(headers[i])
                self.predicates.append('not_' + headers[i])

        for i, row in enumerate(Y):
            # if row[0] > 20000 :
            if row[0] > 1:
                Y[i][0] = 0
            else:
                Y[i][0] = 1

        self.rows = torch.tensor(final_dataset).float()
        self.labels = torch.tensor(Y).float()
        self.variables_number = final_dataset.shape[1]

    def __len__(self):
        return len(self.rows)

    def __getitem__(self, idx):
        example = self.rows[idx]
        label = self.labels[idx]
        return example, label
