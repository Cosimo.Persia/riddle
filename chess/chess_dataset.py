import pickle
import pandas as pd
import torch
import numpy as np
from torch.utils.data import Dataset
from sklearn.preprocessing import KBinsDiscretizer, OneHotEncoder

class Chess_dataset(Dataset):
    def __init__(self,file_name):

        # file_name = 'hepatitis.data'
        headers = [f'pos_{i}' for i in range(36)]
        data = pd.read_csv(file_name, header=None).to_numpy()
        n_feature = 36
        class_position = 36
        Y = data[:, [class_position]]
        non_binary_variable_pos = []  # after having removed the target
        dic_col_bin = {
        }
        data = np.delete(data, [class_position], 1)

        enc = OneHotEncoder(handle_unknown='ignore')
        enc.fit(data)
        data_oh = enc.transform(data).toarray()

        total_length = sum(2*len(e) for e in enc.categories_)
        final_dataset = np.zeros((data.shape[0], total_length))

        for i, row in enumerate(data_oh):
            pos = 0
            for j, v in enumerate(row):
                if v ==1:
                    final_dataset[i][pos] = 1
                else :
                    final_dataset[i][pos+1] = 1
                pos += 2


        self.predicates = []
        for i, e in enumerate(enc.categories_):
            for category in e:
                self.predicates.append(headers[i]+f'_{category}')
                self.predicates.append('not_' + headers[i]+f'_{category}')

        for i, row in enumerate(Y):
            if row[0] == 'won' :
                Y[i][0] = 0
            else:
                Y[i][0] = 1
        #
        Y = Y.astype(np.float32)

        self.rows = torch.tensor(final_dataset).float()
        self.labels = torch.tensor(Y).float()
        self.variables_number = final_dataset.shape[1]

    def __len__(self):
        return len(self.rows)

    def __getitem__(self, idx):
        example = self.rows[idx]
        label = self.labels[idx]
        return example, label


